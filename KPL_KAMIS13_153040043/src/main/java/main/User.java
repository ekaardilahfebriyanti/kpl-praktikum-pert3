package main;

public class User {
    private String name;
    private int age;
    public User(String name, int age){
        if (!textHasContent(name)){
            throw new IllegalArgumentException("Name has no content");
        }
        if (age < Integer.MIN_VALUE || age > Integer.MAX_VALUE){
            throw new IllegalArgumentException("Age not in range[type data integer]: " + age);
        }
        this.name = name;
        this.age = age;
    }
    public String toString(){
        return "Name : "+ name +" , "+" Age : " +age;
    }
    private boolean textHasContent(String text){
        String EMPTY_STRING = "";
        return (text != null) &&
                (!text.trim().equals(EMPTY_STRING));
    }
}
