package main;

public class LatihanFive {
    public int getAbsAdd(int x, int y){
//        assert x != Integer.MIN_VALUE;
//        assert y != Integer.MAX_VALUE;
        if(x == Integer.MIN_VALUE || y == Integer.MIN_VALUE){
            throw new IllegalArgumentException();
        }
        int absX = Math.abs(x);
        int absY = Math.abs(y);
        if(absX > Integer.MAX_VALUE - absY){
            throw new IllegalArgumentException();
        }
        return absX + absY;
    }
}
