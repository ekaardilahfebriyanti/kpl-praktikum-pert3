package main;

public class SubClass extends SuperClass {
    private static String color = null;
    public SubClass(){
        super();
        color = "Red";
    }
    public static String doLogic(){
        return "This is Subclass! The color is : " + color;
    }
}
